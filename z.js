$(function(){
	var slideBox = function(selector) {
		this.box = $(selector);
		this.slider = $(selector).find('[data-role=slider]');
		this.items = this.slider.find('[data-role=item]');
		this.length = this.items.length;
        this.nextBtn = this.box.find('[data-role=next]');
        this.prevBtn = this.box.find('[data-role=prev]');
		this.slideLock = false;
		this.firstNode = 0;
		this.lastNode  = 0;
	};

	slideBox.prototype = {
		init: function() {

			var self  = this;

			// get each item width ,add it in a array
			self.resize();
            this._takeCare();
			self.box.on('click','[data-role=next]',function(){
				self._next();
                self._takeCare();

			});

			self.box.on('click','[data-role=prev]',function() {
				self._prev();

                self._takeCare();
			});
		},

		resize: function() {
			this.showWidth = this.slider.parent().innerWidth();
			this.getWidthDatas(true);
			var extraPadding = Math.floor((this.showWidth - this.maxWidth)/this.maxShowNum);
			this.addPadding(extraPadding+2);
			this.getEachPosition();

		},

		getWidthDatas: function(count) {

			var items =  this.items;
			var temp = [];
			var tempWidth = 0;
			var flag = false;

			for(var i= 0 ;i < this.length; i++) {
				var width = $(items[i]).innerWidth();
				temp.push(width);
				if(count) {
					tempWidth  += width;
					if(!flag && tempWidth > this.showWidth ) {
						this.maxShowNum  = i;
						this.maxWidth = tempWidth-width;
						flag = true;
					}
				}
			}
			this.itemsWidth = temp;
			this.lastNode = this.maxShowNum - 1;

		},

		getEachPosition: function() {

			//before getting each position ,we should calculate the right width after adding extra padding
			this.getWidthDatas();
			var temp = [0];
			for(var i = 1 ;i <= this.length; i++) {
				temp.push(temp[i-1]+this.itemsWidth[i-1]);
			}
			this.pDistance = temp;
		},

		addPadding: function(x) {
			if(x == 0) return;
			x = parseInt(this.items.css('paddingRight'))+x;
			this.items.css('paddingRight',x);
		},

		_next: function() {
			var self = this;
			if(self._lock() || this.nextBtn.hasClass('disable')) {return; }



			var moveDistance =self.pDistance[self.lastNode+2] - self.showWidth;
			
			self.lastNode++;
			self.firstNode++;
			$(self.slider).animate({
				left: -moveDistance
			},300,'linear');
		},

		_prev: function() {
			var self = this;
			if(this._lock() || this.prevBtn.hasClass('disable')) {return; }
			var moveDistance = self.pDistance[self.firstNode-1];
			self.lastNode--;
			self.firstNode--;
			$(self.slider).animate({
				left: -moveDistance
			},300,'linear');
			
		},

		_lock: function() {
			var currentLock = true;
			if(!this.slider.is(':animated') || !this.slideLock) {
				currentLock = false;
			}

			this.slideLock = true;

			return currentLock;
		},

        _takeCare: function() {
            var self =  this;
            console.log(this.lastNode);
            if(self.lastNode > self.length-2) {
                this.nextBtn.addClass('disable');
            } else{
                this.nextBtn.removeClass('disable');
            }
            if(self.firstNode < 1) {
                this.prevBtn.addClass('disable');
            }else {
                this.prevBtn.removeClass('disable')
            }
        }

	};



	var a = new slideBox('#wow');
	a.init();
});